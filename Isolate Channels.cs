// Name: Moof's Isolate Channels v1.0
// Submenu: Color
// Author: MoofEMP
// Title: Isolate Channels
// Version: 1.0
// Desc: Returns a single channel of the selection in greyscale
// URL: https://gitlab.com/moofemp-pdn-plugins/isolate-channels
#region UICode
RadioButtonControl ChannelOption = 0; // Channel to isolate|Red|Green|Blue|Alpha|Hue|Saturation|Value
#endregion

void Render(Surface dst,Surface src,Rectangle rect)
{
    ColorBgra currentPixel;
    for(int y=rect.Top;y<rect.Bottom;y++){
        if(IsCancelRequested) return;
        for(int x=rect.Left;x<rect.Right;x++){
            currentPixel=src[x,y];
            if(ChannelOption<=3){
                switch(ChannelOption){
                    case 0: //Red
                        currentPixel.G=currentPixel.R;
                        currentPixel.B=currentPixel.R;
                        break;
                    case 1: //Green
                        currentPixel.R=currentPixel.G;
                        currentPixel.B=currentPixel.G;
                        break;
                    case 2: //Blue
                        currentPixel.R=currentPixel.B;
                        currentPixel.G=currentPixel.B;
                        break;
                    case 3: //Alpha
                        currentPixel.R=currentPixel.A;
                        currentPixel.G=currentPixel.A;
                        currentPixel.B=currentPixel.A;
                        break;
                }
            }
            else{
                HsvColor pxhsv=HsvColor.FromColor(currentPixel.ToColor());
                byte newValue=0;
                switch(ChannelOption){
                    case 4: //Hue
                        newValue=(byte)(pxhsv.Hue*17/24); //Hue returns 0-360, remap to 0-255
                        break;
                    case 5: //Saturation
                        newValue=(byte)(pxhsv.Saturation*51/20); //Saturation returns 0-100, remap to 0-255
                        break;
                    case 6: //Value
                        newValue=(byte)(pxhsv.Value*51/20); //Value returns 0-100, remap to 0-255
                        break;
                }
                currentPixel.R=newValue;
                currentPixel.G=newValue;
                currentPixel.B=newValue;
            }
            currentPixel.A=255;
            dst[x,y]=currentPixel;
        }
    }
}
