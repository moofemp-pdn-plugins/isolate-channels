# Isolate Channels for Paint.NET

Returns a single channel of the selection in greyscale

[`Isolate Channels.cs`](Isolate%20Channels.cs) is the source code  
[`Isolate Channels.dll`](Isolate%20Channels.dll) is the compiled .dll  
[`logo.png`](logo.png) is the effect icon

**INSTALLATION INSTRUCTIONS:**  
You only need to download [`Isolate Channels.dll`](Isolate%20Channels.dll) and place it in `C:\Program Files\paint.net\Effects`

Created with CodeLab: https://boltbait.com/pdn/codelab/

Paint.NET: https://www.getpaint.net/

## Sample Images

Click any of these to enlarge; they're much bigger than shown here.

Original image  
<img src="samples/orig.png" alt="orig" width=150>

Red channel isolated  
<img src="samples/red.png" alt="red" width=150>

Green channel isolated  
<img src="samples/green.png" alt="green" width=150>

Blue channel isolated  
<img src="samples/blue.png" alt="blue" width=150>

Alpha channel isolated  
<img src="samples/alpha.png" alt="alpha" width=150>

**With HSV conversion:**

Hue channel isolated  
<img src="samples/hue.png" alt="hue" width=150>

Saturation channel isolated  
<img src="samples/saturation.png" alt="saturation" width=150>

Value channel isolated  
<img src="samples/value.png" alt="value" width=150>
